import wave
import time
import datetime
import math
import threading
from abc import abstractmethod
import numpy as np
import io

from audioapp.client.base import BaseStream
from audioapp.context import config
from audioapp.utils.logger import get_logger
from audioapp.client.utils import random_wiki_page
from audioapp.festival.festival_client import FestivalClient

logger = get_logger(__name__)


class BaseSource(BaseStream):

    def __init__(self, name, position, *args, **kwargs):
        super(BaseSource, self).__init__(*args, **kwargs)
        self.active_stream = False
        self.position = position
        self.name = name

    def on_connect(self):
        logger.info(f'Source {type(self).__name__} {self.name} connected!')
        self.sio.emit('new_audiosource', {'name': self.name,
                                          'channels': self.channels,
                                          'byterate': self.byterate,
                                          'frequency': self.frequency})

    def on_start_streaming(self, data):
        self.active_stream = True
        self.streaming_thread = threading.Thread(target=self.stream)
        self.streaming_thread.start()
        # MG where to join this?
        self.streaming_thread.join()

    @abstractmethod
    def stream():
        pass

class Bot(BaseSource):

    def __init__(self, *args, **kwargs):
        super(Bot, self).__init__(*args, **kwargs)

        self.festival_client = FestivalClient(config.FESTIVAL_HOST, config.FESTIVAL_PORT)
        # If we can't connect, just spit out error and abort
        if not self.festival_client.connect():
            raise RuntimeError(f'Cannot connect to festival server {config.FESTIVAL_HOST} on port {config.FESTIVAL_PORT}.')

        self.channels = 1
        self.byterate = 2
        self.frequency = 16000 # depends on the voice
        self.CHUNK = int(config.T * self.frequency * 2) # MG see below.

        logger.debug(f"FileStream : N of frames in audio chunk {self.CHUNK}, size: {self.CHUNK * self.byterate / 1024} kb.")


    def ttw(self, text):
        message = f'(tts_textall "{text}" nil)\n;'
        _, wav = self.festival_client.send_message(message.encode())
        return wav[0]

    def list_all_voices(self):
        """
        List all voices on the server
        """
        message = '(voice.list)\n'
        scheme_responses, _ = self.festival_client.send_message(message.encode())
        scheme_responses = [line.decode('ascii') for line in scheme_responses]
        list_of_voices = scheme_responses[0].replace('(','').replace(')','').split(' ')
        return list_of_voices

    def stream(self):

        text = random_wiki_page()
        text = "Welcome to audio app."

        wav = io.BytesIO(self.ttw(text))

        t, data = 0, wav.read(self.CHUNK)
        while (data != b'' and data is not None) and self.active_stream:
            self.position = [2*math.sin(math.radians(t)), 0, 0 ]
            # Send the next chunk
            self.sio.emit('streamdata', {'timestamp': datetime.datetime.utcnow().strftime(config.DATETIME_FORMAT),
                                         'name': self.name, 'chunk': data.decode('ISO-8859-1'),
                                         'position': self.position})
            t, data = t + 1, wav.read(self.CHUNK)

            # During playback, if the application does not pass data into the [circular] buffer quickly enough
            # it becomes starved for data, resulting in an error called underrun.
            # The factor 0.9 prevents exactly this.
            # MG it does not really work. What really works is to increase the number of frames.
            time.sleep(config.T * 0.9)

        self.active_stream = False
        self.sio.emit('end_of_streaming', {'name': self.name})

class FileStream(BaseSource):

    def __init__(self, wavfile, name=None, *args, **kwargs):

        if name is None:
            name = wavfile

        super(FileStream, self).__init__(name, *args, **kwargs)

        self.wf = wave.open(wavfile, 'rb')
        self.name = name
        self.channels, self.byterate, self.frequency = self.wf.getnchannels(), self.wf.getsampwidth(), self.wf.getframerate()

        # In a live stream, this should be
        # self.CHUNK = self.frequency * config.T
        # But here it can be anything
        self.CHUNK = int(self.frequency * config.T )

        logger.debug(f"FileStream : N of frames in audio chunk {self.CHUNK}, size: {self.CHUNK * self.byterate / 1024} kb.")

    def stream(self):

        t, data = 0, self.wf.readframes(self.CHUNK)
        while (data != b'' and data is not None) and self.active_stream:
            self.position = [2*math.sin(math.radians(t)), 0, 0 ]
            # Send the next chunk
            self.sio.emit('streamdata', {'timestamp': datetime.datetime.utcnow().strftime(config.DATETIME_FORMAT),
                                         'name': self.name, 'chunk': data.decode('ISO-8859-1'),
                                         'position': self.position})
            t, data = t + 1, self.wf.readframes(self.CHUNK)

            # During playback, if the application does not pass data into the [circular] buffer quickly enough
            # it becomes starved for data, resulting in an error called underrun.
            # The factor 0.9 prevents exactly this.
            # MG it does not really work. What really works is to increase the number of frames.
            time.sleep(config.T * 0.9)

        self.active_stream = False
        self.sio.emit('end_of_streaming', {'name': self.name})
