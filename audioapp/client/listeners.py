import pyaudio
from openal.audio import StreamingSoundData, SoundListener, SoundSink, SoundData

from audioapp.client.base import BaseStream, ExtSoundSource
from audioapp.utils.logger import get_logger
from audioapp.client.utils import noalsaerr

logger = get_logger("client")

# suppress annoying startup alsalib logs
with noalsaerr():
    p = pyaudio.PyAudio()


class BaseListener(BaseStream):

    def __init__(self, name, *args, **kwargs):
        self.name = name
        super(BaseListener, self).__init__(*args, **kwargs)

    def on_connect(self):
        logger.info(f'Listener {type(self).__name__} {self.name} connected!')
        self.sio.emit('new_listener')

    def on_new_source(self, source):
        logger.warning((f"New source: {source['name']}: "
                        f"channels: {source['channels']}, "
                        f"bitrate: {source['byterate']}, "
                        f"frequency: {source['frequency']}"))


class ZeroDListener(BaseListener):

    def __init__(self, *args, **kwargs):
        super(ZeroDListener, self).__init__(*args, **kwargs)

        self.pstreams = {}

    def on_new_source(self, source):
        super(ZeroDListener, self).on_new_source(source)
        self.pstreams[source['name']] = p.open(format=p.get_format_from_width(source['byterate']),
                                               channels=source['channels'],
                                               rate=source['frequency'],
                                               output=True)
        logger.info(f'Stream opened for {source["name"]}')
        self.sio.emit('stream_opened', {'name': source['name']})

    def on_streamdata(self, data):
        source_name = data['name']
        chunk = bytes(data['chunk'], 'ISO-8859-1')

        # TODO Maaaaybe we should collect enough chunks in a queue before writing them?

        # During playback, if the application does not pass data into the [circular] buffer quickly enough,
        # it becomes starved for data, resulting in an error called underrun.

        # Sometimes I also get [Errno -9999] Unanticipated host error. Maybe related to underrrun?
        try:
            self.pstreams[source_name].write(chunk)
        except BaseException as err:
            logger.error(f"Chunk length: {len(chunk)}")
            logger.error(err)

    # TODO create event in the server?
    def on_end_of_streaming(self, source):
        name = source['name']
        logger.info(f'Stream closed for {name}')
        self.pstreams[name].stop_stream()
        self.pstreams[name].close()


class ThreeDListener(BaseListener):

    def __init__(self, *args, **kwargs):
        super(ThreeDListener, self).__init__(*args, **kwargs)
        self.sources = {}
        self.listener = SoundListener()
        self.sink = SoundSink()
        self.sink.activate()

    def on_new_source(self, source):
        super(ThreeDListener, self).on_new_source(source)
        newsource = ExtSoundSource(name=source['name'],
                                   channels=source['channels'],
                                   frequency=source['frequency'],
                                   byterate=source['byterate'])
        self.sources[source['name']] = newsource
        self.sink.play(source['name'])

    def on_streamdata(self, data):

        if data['name'] not in self.sources.keys():
            logger.error(f"Unknow source {data['name']}")
            return
        if data['chunk'] is None: return

        source = self.sources[data['name']]
        sounddata = SoundData(data=data['chunk'], channels=source.channels, bitrate=source.byterate * 8, frequency=source.frequency, size=len(data['chunk']))
        source.position = data['position']
        source.sound.queue(sounddata)
        logger.warning(f"{source.name} : {source.position}")

    def play(self):
        self.sink.update()
