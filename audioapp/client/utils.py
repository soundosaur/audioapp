from contextlib import contextmanager
from ctypes import c_char_p, c_int, CFUNCTYPE, cdll

from audioapp.utils.logger import get_logger
logger = get_logger(__name__)

ERROR_HANDLER_FUNC = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)


def py_error_handler(filename, line, function, err, fmt):
    # TODO define another log level below DEBUG?
    pass


c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)


@contextmanager
def noalsaerr():
    asound = cdll.LoadLibrary('libasound.so')
    asound.snd_lib_error_set_handler(c_error_handler)
    yield
    asound.snd_lib_error_set_handler(None)


def random_wiki_page(lang='en'):

    try:
        import wikipedia
        wikipedia.set_lang(lang)

        random_page = wikipedia.random(1)
        page = wikipedia.page(random_page)
    except ImportError:
        logger.warning('Cannot import the wikipedia module.')
        page = 'Please install the wikipedia pip package!'
    except BaseException:
        page = 'Cannot Connect to Wikipedia'
    return page
