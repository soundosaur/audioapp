import socketio
from audioapp.utils.logger import get_logger

logger = get_logger(__file__)


class BaseStream(socketio.ClientNamespace):

    def __init__(self,
                 sio,
                 position=[0, 0, 0], orientation=[0, 1, 0],
                 *args, **kwargs):

        super(BaseStream, self).__init__(*args, **kwargs)

        self.sio = sio
        self.position = position
        self.orientation = orientation

    def on_connect(self):
        logger.info('connection established')

    def on_disconnect(self):
        logger.info('disconnected from server')

class ExtSoundSource():
    def __init__(self, name, channels, frequency, byterate, *args, **kwargs):
        self.sound = SoundSource(*args, **kwargs)
        self.name = name
        self.channels = channels
        self.frequency = frequency
        self.byterate= byterate