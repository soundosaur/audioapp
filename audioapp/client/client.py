import time
import logging
import socketio
import requests

from audioapp.client.listeners import ZeroDListener
from audioapp.client.sources import FileStream, Bot
from audioapp.context import config, DATA
from audioapp.utils.logger import get_logger

logger = get_logger(__name__)
loggers_dict = logging.Logger.manager.loggerDict
logging.getLogger('urllib3').setLevel(logging.ERROR)
logging.getLogger('socketio').setLevel(logging.ERROR)
logging.getLogger('engineio').setLevel(logging.ERROR)


def simulate_call():

    listener_sio = socketio.Client()
    listener = ZeroDListener(name='Listener',
                             namespace='/listeners',
                             sio=listener_sio)
    listener_sio.register_namespace(listener)

    source_sio = socketio.Client()
    sources = [FileStream(namespace='/sources/Piano',
                          wavfile=(DATA / 'piano.wav').as_posix(),
                          name='Piano',
                          position=[2, 0, 0],
                          sio=source_sio),
               Bot(namespace='/sources/DummyLeTont',
                   name='DummyLeTont',
                   position=[2, 0, 0],
                   sio=source_sio),]
            #    FileStream(namespace='/sources/Organ',
            #               wavfile=(DATA / 'organ.wav').as_posix(),
            #               name='Organ',
            #               position=[-2, 0, 0],
            #               sio=source_sio),]

    for source in sources: source_sio.register_namespace(source)

    response = session.post(f'http://{config.APP_HOST}:{config.APP_PORT}/login',
                            json={'username': 'matteo', 'password': '123'})

    assert response.return_code == 302
    listener_sio.connect(f'http://{config.APP_HOST}:{config.APP_PORT}/connect')
    source_sio.connect(f'http://{config.APP_HOST}:{config.APP_PORT}/connect')
    listener_sio.wait()
    source_sio.wait()


def connect():
    while True:
        try:
            simulate_call()
        except ConnectionError:
            logger.error(f'Connection refused by the server {config.APP_HOST} port {config.APP_PORT}.')
            logger.error(f'Retrying...')
            time.sleep(0.1)


if __name__ == "__main__":
    connect()
