from pathlib import Path
from pydantic import BaseSettings, validator, PostgresDsn, HttpUrl
from typing import Any, Dict, Optional, List
import secrets
ROOT = Path(__file__).absolute().parents[1]
DATA = ROOT / 'data'


class Config(BaseSettings):
    """App config"""

    APP_NAME: str = 'SoundoSaur'
    APP_SECRET: str = secrets.token_urlsafe(16)
    APP_HOST: str = 'localhost'
    APP_PORT: int = 5002

    DB_USER: str = None
    DB_HOST: str = None
    DB_PASSWORD: str = None
    DB_PORT: str = None
    DB_NAME: str = None

    SQLALCHEMY_DATABASE_URI: PostgresDsn = None

    # to enable gevent support in the debugger.
    GEVENT_SUPPORT: bool = True

    LOG_LEVEL: str = 'DEBUG'

    # Time length of a package in the live audio stream
    T: int = 2  # MG why is this an int and not a float?

    # Festival
    FESTIVAL_HOST: str = 'localhost'
    FESTIVAL_PORT: int = 1314

    DATETIME_FORMAT = "%Y%m%dT%H%M%S%f"

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, url: Optional[str], values: Dict[str, Any]) -> Any:
        """
        validate/generate db url
        """
        if isinstance(url, str):
            return url
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("DB_USER"),
            password=values.get("DB_PASSWORD"),
            host=values.get("DB_HOST"),
            port=values.get("DB_PORT"),
            path=f"/{values.get('DB_NAME') or ''}",
        )

    class Config:
        env_file = ROOT / ".env"


config = Config()
