"""
Funtions to standardize logging.
"""

import logging
import sys
from logging.handlers import TimedRotatingFileHandler

LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
FORMATTER = logging.Formatter(LOG_FORMAT)

def get_console_handler(stream, formatter):
   console_handler = logging.StreamHandler(stream)
   console_handler.setFormatter(formatter)
   return console_handler

def get_file_handler(logfile, formatter):
   file_handler = TimedRotatingFileHandler(logfile, when='midnight')
   file_handler.setFormatter(formatter)
   return file_handler

def get_logger(logger_name, level=logging.DEBUG, logfile=None, consolestream=sys.stdout, formatter=FORMATTER):
   logger = logging.getLogger(logger_name)
   logger.setLevel(level) # better to have too much log than not enough

   if consolestream is not None:
        logger.addHandler(get_console_handler(consolestream, formatter))

   if logfile:
        logger.addHandler(get_file_handler(logfile, formatter))
   # with this pattern, it's rarely necessary to propagate the error up to parent
   logger.propagate = False
   return logger