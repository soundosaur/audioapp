import functools

from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from flask import current_app, Blueprint, request, redirect, url_for
from werkzeug.urls import url_parse
from flask_login import LoginManager
from flask_login import LoginManager, current_user, login_user, logout_user, login_required
from flask_socketio import disconnect
from audioapp.context import config, DATA
from audioapp.utils.logger import get_logger
from audioapp.server.database.models import User
from flask import session, jsonify

logger = get_logger(__name__)


def authenticated_only(f):
    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        if not current_user.is_authenticated:
            auth = request.authorization
            if auth is not None:
                username, password = auth['username'], auth['password']
                user = User.query.filter_by(username=username).first()
                if user is not None and user.check_password(password):
                    return user
            logger.error("Current user not authenticated, disconnecting.")
            disconnect()
        else:
            return f(*args, **kwargs)
    return wrapped


def verify_auth_token(token):
    s = Serializer(current_app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None  # valid token, but expired
    except BadSignature:
        return None  # invalid token
    user = User.query.filter_by(id=data['id']).first()
    return user
