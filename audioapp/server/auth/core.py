from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from flask import current_app, Blueprint, request, redirect, url_for, jsonify
from werkzeug.urls import url_parse
from flask_login import LoginManager
from flask_login import LoginManager, current_user, login_user, logout_user, login_required
from flask_socketio import disconnect
from audioapp.context import config, DATA
from audioapp.utils.logger import get_logger
from audioapp.server.database.models import User
from audioapp.server.auth.utils import verify_auth_token


auth = Blueprint('auth', __name__, url_prefix='/auth')
login_manager = LoginManager()
login_manager.login_view = 'auth.login'

logger = get_logger(__name__)


@auth.route('/login', methods=['POST', 'GET'])
def login():

    username = request.json['username']
    password = request.json['password']

    if current_user.is_authenticated:
        logger.warning(f'User {current_user.username} already authenticated.')
        if current_user.username != username:
            logger.error('Please logout before logging in again with a different user.')
        return redirect(url_for('index')), 401

    user = User.query.filter_by(username=username).first()
    if user is None or not user.check_password(password):
        logger.warning(f'Invalid username or password for username {username}.')
        return redirect(url_for('auth.login')), 401

    login_user(user, remember=True)
    logger.warning(f'Successful login for user {username}.')
    next_page = request.args.get('next')
    if not next_page or url_parse(next_page).netloc != '':
        next_page = url_for('index')
    return redirect(next_page)


@auth.route("/logout")
@login_required
def logout():
    logger.warning(f'Logging out user {current_user.username}.')
    logout_user()
    return redirect(url_for('index'))


@auth.route('/token')
@login_required
def get_auth_token():
    logger.warning(f'Creating auth token for user {current_user.username}.')
    token = current_user.generate_auth_token()
    return jsonify({'token': token.decode('ascii')})


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(id=user_id).first()


@login_manager.request_loader
def load_request(request):
    auth = request.authorization
    if auth is not None:
        username_or_token, password = auth['username'], auth['password']

        # We first try to authenticate via token
        user = verify_auth_token(token=username_or_token)
        if user: return user

        user = User.query.filter_by(username=username_or_token).first()
        if user is not None and user.check_password(password):
            return user

    return None
