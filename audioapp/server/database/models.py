import re
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from flask_sqlalchemy import SQLAlchemy
from flask import current_app
from flask_login import UserMixin
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method

# from audioapp.server.database import db
db = SQLAlchemy()


class User(db.Model, UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email_address = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    # is_authenticated = db.Column(db.Boolean, default=False)
    is_active = db.Column(db.Boolean, default=True)
    is_anonymous = db.Column(db.Boolean, default=False)

    # def __init__(self):
    #     self.is_authenticated = False

    def __repr__(self):
        return f"<User(username='{self.username}', email='{self.email}')>"

    @hybrid_property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, plain_text_password):
        self.password_hash = generate_password_hash(plain_text_password)

    @hybrid_property
    def email(self):
        return self.email_address

    @email.setter
    def email(self, email_address_candidate):
        if re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email_address_candidate) is None:
            raise ValueError(f'Wrong email field "{email_address_candidate}" in user.')

        self.email_address = email_address_candidate

    @hybrid_method
    def check_password(self, plain_text_password):
        return check_password_hash(self.password, plain_text_password)

    def generate_auth_token(self, expiration=600):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})