from flask_sqlalchemy import SQLAlchemy
# MG For reasons behind my understanding this is the only way of making this work with the initialize_db.
from audioapp.server.database.models import db


def initialize_db():

    from audioapp.server.app import create_app
    from audioapp.server.database.models import User

    app, _ = create_app()

    with app.app_context():
        db.create_all()
        db.session.commit()

        mg = User(username='matteo', password='123')
        db.session.add(mg)
        db.session.commit()
        pass


if __name__ == "__main__":
    initialize_db()