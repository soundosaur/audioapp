from flask_socketio import join_room, leave_room, SocketIO, ConnectionRefusedError, send, emit, disconnect
from flask_login import current_user, login_user
from flask import session
import datetime

from audioapp.utils.logger import get_logger, FORMATTER
from audioapp.server.auth.utils import authenticated_only
from audioapp.server.database.models import User
from audioapp.context import config


logger = get_logger(__name__)

socketio = SocketIO(logger=logger, manage_session=False)  # async_mode='threading'


@socketio.on_error()
def error_handler(e):
    logger.error(e)


@socketio.on('get-session')
def get_session():
    emit('refresh-session', {
        'session': session.get('value', ''),
        'user': current_user.username if current_user.is_authenticated else 'anonymous'
    })

# @socketio.on('join')
# def on_join(data):
#     username = data['username']
#     room = data['room']
#     join_room(room)
#     logger.info(f'{username} has entered the room {room}.')
#     send(f'Welcome {username} in the room {room}.', room=room)

# @socketio.on('leave')
# def on_leave(data):
#     username = data['username']
#     room = data['room']
#     leave_room(room)
#     logger.info(f'{username} has left the room {room}.')
#     send(f'Goodbye user {username}.', room=room)


@socketio.on('connect')
def connect_handler():

    logger.debug(f'{current_user} connected to the websocket.')
    if type(current_user.is_anonymous) == bool and current_user.is_anonymous:
        disconnect()
        return

    # We see if the user exists already in the session.
    # That can be the case if he logged in via requests.
    is_user_already_in_session = '_user_id' in session and session['_user_id'] == str(current_user.id)
    username = current_user.username
    if current_user.is_authenticated and not is_user_already_in_session:
        user = User.query.filter_by(username=username).first()
        success = login_user(user, remember=True)
        assert success is True
        logger.warning(f'Successful login for user {user.username} via websocket.')

    emit(f'Welcome {username}.')

    # username = current_user.username
    # success = login_user(current_user, remember=True)

    # BUG? the application crashes badly
    # if current_user.is_authenticated:
    #     logger.warning(f'User {username} already authenticated.')
    # else:
    # for client in app.clients:
    #     emit('open_audiostream', {'channels' : client.channels, 'byterate' : client.byterate, "frequency" : client.frequency})
    # app.register_new_connection()
    # app.client_connected = True


@socketio.on('new_audiosource')
@authenticated_only
def announce_new_source(data):
    logger.info('New source announced!')
    emit('new_source', data, broadcast=True, namespace='/listeners')  #, include_self=False)


@socketio.on('new_listener')
@authenticated_only
def announce_new_listener():
    logger.info('New listener announced!')


@socketio.on('streamdata')
@authenticated_only
def received_and_broadcast_streamdata(data):

    # syncronization: drop old packages.
    now = datetime.datetime.utcnow()
    timestamp = datetime.datetime.strptime(data['timestamp'], config.DATETIME_FORMAT)

    if (now - timestamp) < datetime.timedelta(seconds=10 * config.T):
        emit('streamdata', data, broadcast=True, namespace='/listeners') #, include_self=False)


@socketio.on('stream_opened')
@authenticated_only
def instruct_source_to_stream(data):
    emit('start_streaming', data, broadcast=True, namespace=f'/sources/{data["name"]}') #, include_self=False)


@socketio.on('end_of_streaming')
def end_of_stream(data):
    # logger.info(f'End of streaming {data["name"]}')
    emit('end_of_streaming', data, broadcast=True, namespace='/listeners') #, include_self=False)

# @socketio.on('start_streaming')
# def streaming_handler():
#     logger.info('Start streaming')

#     for client in app.clients:
#         i, data = 0, client.stream()
#         # TODO decouple the position from the audio data, no point in sending the position all the time.
#         emit('stream_data', {'client': client.name, 'position' : client.position, 'chunk' : data, 'nr' : i})
#         while data != b'' and app.client_connected:
#             i+=1
#             data = client.stream()

#             emit('stream_data', {'client': client.name, 'position' : client.position, 'chunk' : data, 'nr' : i})
#             time.sleep(config.T)
#         logger.info(f'Streamed {i} chunks of size {client.CHUNK}.')
#         emit('end_of_streaming')
#         logger.info('Streaming stopped.')


@socketio.on('disconnect')
def disconnect_handler():
    logger.info(f'User {current_user} disconnected.')