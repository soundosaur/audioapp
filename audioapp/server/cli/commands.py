from flask import current_app, Blueprint, request, redirect, url_for, jsonify
from flask.cli import with_appcontext
import click

from audioapp.server.database.models import User
from audioapp.server.database.models import db

from audioapp.utils.logger import get_logger


logger = get_logger(__name__)
cmd = Blueprint('commands', __name__)


# flask user create demo
@cmd.cli.command('create')
@click.argument('username')
@click.argument('email')
@click.argument('password')
@with_appcontext
def create_user(username, email, password):

    db.create_all()
    db.session.commit()

    # username/mail should be unique.
    user = User.query.filter_by(username=username).first()
    if user:
        logger.warning(f'{user} already exists in database. Skipping.')
        return
    user = User.query.filter_by(email=email).first()
    if user:
        logger.warning(f'{user} already exists in database. Skipping.')
        return

    try:
        new_user = User(username=username, email=email, password=password)
        db.session.add(new_user)
        db.session.commit()
        logger.info(f'Created {new_user}.')

    except BaseException as err:
        logger.error(err)
