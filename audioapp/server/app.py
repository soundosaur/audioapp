"""
A Flask audio application that makes uses of websockets.
"""


from flask import Flask, redirect, url_for, request, render_template, logging as flog, request, session, jsonify

from flask_sockets import Sockets
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user, login_user, logout_user, login_required
from flask_session import Session
import logging
import wave
import time
import math
import datetime

from audioapp.context import config, DATA
from audioapp.server.database import db
from audioapp.server.database.models import User
from audioapp.server.auth.core import auth, login_manager
from audioapp.server.cli.commands import cmd
from audioapp.server.socket.io import socketio
from audioapp.utils.logger import get_logger, FORMATTER

logger = get_logger(config.APP_NAME)

session_manager = Session()


# loggers_dict = logging.Logger.manager.loggerDict

class Server(Flask):

    def __init__(self, *args, **kwargs):
        super(Server, self).__init__(*args, **kwargs)

    def register_new_connection(self):
        """
        A new client just connected (stub)
        """
        logger.warning('New Connection!')


def create_app(config=config):

    app = Server(__name__)
    app.config['SECRET_KEY'] = config.APP_SECRET
    app.config['SESSION_TYPE'] = 'filesystem'

    login_manager.init_app(app)
    app.register_blueprint(auth)
    app.register_blueprint(cmd)

    session_manager.init_app(app)

    socketio.init_app(app)

    app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # adds overhead

    db.init_app(app)

    logging.getLogger('socketio').setLevel(logging.ERROR)
    logging.getLogger('engineio').setLevel(logging.ERROR)
    # MG to check all loggers: loggers_dict = logging.Logger.manager.loggerDict
    app.logger.setLevel(config.LOG_LEVEL)
    flog.default_handler.setFormatter(FORMATTER)

    @app.route('/')
    @app.route('/index')
    # @login_required
    def index():
        return 'AudioApp :)'

    @app.route('/connect')
    @login_required
    def connect():
        return render_template('socket.html')

    @app.route('/session')
    @login_required
    def session_access():
        return jsonify({
            'session': session.get('value', ''),
            'user': current_user.username if current_user.is_authenticated else 'anonymous'
        })

    return app


def start_server():

    app = create_app(config)

    # Note that socketio.run(app) runs a production ready server
    # only when eventlet or gevent are installed. Otherwise it runs on the flask dev server,
    # which is not appropriate.
    # socketio.run(app, debug=True, host='0.0.0.0', port=config.APP_PORT, logger=False, engineio_logger=False)
    socketio.run(app, debug=True, host='0.0.0.0', port=config.APP_PORT)

    # from gevent import pywsgi
    # from geventwebsocket.handler import WebSocketHandler
    # server = pywsgi.WSGIServer(('', config.APP_PORT), app, handler_class=WebSocketHandler)
    # server.serve_forever()

    app.logger.info(f"Server listening on: http://{config.APP_HOST}:{config.APP_PORT}")

    # TODO in docker use gunicorn --worker-class eventlet -w 1 module:app
    # or                 gunicorn -k gevent -w 1 module:app


if __name__ == "__main__":
    start_server()
