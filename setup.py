from setuptools import setup, find_packages


def readme():
    # read the contents of your README file
    from os import path
    this_directory = path.abspath(path.dirname(__file__))
    with open(path.join(this_directory, 'Readme.md'), encoding='utf-8') as f:
        long_description = f.read()
    return long_description


setup(
    name='audioapp',
    version='0.1.0',
    license='commercial license',
    description='Audio experiments',
    long_description=readme(),
    long_description_content_type='text/markdown',
    author='Matteo Giani <matteo.giani.87@gmail.com>',
    packages=["audioapp"],  # find_packages(where=".", exclude=("tests",)),
    python_requires='>=3.5',
    install_requires=[
        'python-openal',
        'flask',
        'flask_socketio',
        'flask_login',
        'flask_sockets',
        'flask_session',
        'flask_sqlalchemy',
        'flask_sqlalchemy_session',
        'psycopg2',
        'pydantic',
        'python-dotenv',
        'pyaudio',
        'requests',
        'websockets',
        'numpy',
        'click',
    ],
    extras_require={
        'dev': ['pylama', 'pytest'],
        'bot': ['wikipedia']
    },
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'audioclient = audioapp.client.client:connect',
            'audioserver = audioapp.server.app:start_server'],
        'flask.commands': [
            'create_user=audioapp.server.cli.commands:create_user'
        ],
    }
)
