import pytest

from audioapp.server.app import create_app
from audioapp.server.database import db
from audioapp.server.database.models import User
from audioapp.context import Config
from audioapp.utils.logger import get_logger
import logging

logger = get_logger(__name__)

test_username = 'test'
test_pwd = '1234'


@pytest.fixture(scope='session')
def add_test_user():
    with app.app_context():
        db.create_all()
        db.session.commit()

        tu = User(username=test_username, password=test_pwd)
        db.session.add(tu)
        db.session.commit()
        logger.info(f'Created testuser "{test_username}"')
    return test_username, test_pwd


@pytest.fixture(scope='session', autouse=True)
def set_log_levels():

    logger.debug(f"Loggers {logging.root.manager.loggerDict.keys()}")

    for _logger in ["urllib3", "Fiona", "fiona", "shapely", "imageio", 'matplotlib', "PIL"]:
        logger.debug(f'Setting minimum level of "warning" for logger {_logger}.')
        logging.getLogger(_logger).setLevel(logging.WARNING)

    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S')


class TestingConfig(Config):
    SQLALCHEMY_DATABASE_URI: str = 'sqlite:///:memory:'
    SHOW_QUERIES: bool = False


app = create_app(config=TestingConfig())


@pytest.fixture
def app_client(add_test_user):

    with app.app_context():
        yield app.test_client()
