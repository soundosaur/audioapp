
import pytest

from audioapp.context import config
from audioapp.utils.logger import get_logger
from audioapp.server.socket.io import socketio
from utils import app, app_client, add_test_user, test_pwd, test_username
from base64 import b64encode

userAndPass = b64encode(bytes(f"{test_username}:{test_pwd}", encoding='utf-8')).decode("ascii")
authheaders = {'Authorization': f'Basic {userAndPass}'}

logger = get_logger('tests')


@pytest.mark.unit
def test_unsuccessful_request_login(app_client):
    r = app_client.post('/auth/login', json={'username': 'nonexistentuser', 'password': '123'})
    assert r.status_code == 401


@pytest.mark.unit
def test_unsuccessful_socketio_login(app_client):

    headers = {'Authorization': f'Basic {b64encode(b"nonexistentuser:123").decode("ascii")}'}

    socketio_client = socketio.test_client(app, flask_test_client=app_client, headers=headers)

    assert not socketio_client.is_connected(), "Client not disconnected."

    # received = socketio_client.get_received()
    # assert len(received) == 0, "Received a welcome message."


@pytest.mark.unit
def test_successful_request_login(app_client, add_test_user):
    # log in via HTTP
    r = app_client.post('/auth/login', json={
        'username': test_username, 'password': test_pwd})
    assert r.status_code == 302


@pytest.mark.unit
def test_double_request_login_attempt(app_client, add_test_user):
    # log in via HTTP
    r = app_client.post('/auth/login', json={
        'username': test_username, 'password': test_pwd})
    assert r.status_code == 302

    r = app_client.post('/auth/login', json={
        'username': 'otheruser', 'password': test_pwd})
    assert r.status_code == 401


@pytest.mark.unit
def test_successful_socketio_login_with_auth_headers(app_client, add_test_user):

    socketio_client = socketio.test_client(app, flask_test_client=app_client, headers=authheaders)
    received = socketio_client.get_received()
    assert len(received) == 1 and received[0]['name'] == f'Welcome {test_username}.', "Received no welcome message."


@pytest.mark.unit
def test_shared_session_via_requests(app_client, add_test_user):

    r = app_client.post('/auth/login', json={'username': test_username, 'password': test_pwd})
    assert r.status_code == 302

    socketio_client = socketio.test_client(app, flask_test_client=app_client)
    session_received_via_websocket = socketio_client.get_received()
    assert session_received_via_websocket[0]['name'] == f'Welcome {test_username}.', "Received no welcome message."

    socketio_client.emit('get-session')
    session_received_via_request = app_client.get('/session')
    session_received_via_websocket = socketio_client.get_received()

    current_username_via_socketio = session_received_via_websocket[0]['args'][0]['user']
    current_username_via_request = session_received_via_request.json['user']

    assert current_username_via_socketio == test_username and current_username_via_request == current_username_via_socketio, "Logged in via request: no shared session."


@pytest.mark.skip(reason='Apparently this route does not work :(')
def test_shared_session_via_socketio(app_client, add_test_user):

    socketio_client = socketio.test_client(app, flask_test_client=app_client, headers=authheaders)
    session_received_via_websocket = socketio_client.get_received()
    assert session_received_via_websocket[0]['name'] == f'Welcome {test_username}.', "Received no welcome message."

    socketio_client.emit('get-session')
    session_received_via_request = app_client.get('/session')
    session_received_via_websocket = socketio_client.get_received()

    current_username_via_socketio = session_received_via_websocket[0]['args'][0]['user']
    current_username_via_request = session_received_via_request.json['user']

    assert current_username_via_socketio == test_username and current_username_via_request == current_username_via_socketio, "Logged in via request: no shared session."


@pytest.mark.unit
def test_nonauthorized_connection(app_client):

    socketio_client = socketio.test_client(app, flask_test_client=app_client)
    assert not socketio_client.is_connected(), "Client not disconnected."

    # received = socketio_client.get_received()
    # assert len(received) == 0, "The unauthorized client did in fact receive a welcome message."


@pytest.mark.unit
def test_token(app_client):
    # response = app_client.post('/auth/token', json={'username': test_username, 'password': test_pwd})
    response = app_client.get('/auth/token', headers=authheaders)
    assert response.status_code == 200, f"API did returned a status code of {response.status_code} instad of 200."
    token = response.json['token']

    token_enc = b64encode(bytes(f"{token}:", encoding='utf-8')).decode("ascii")
    authheaders_token = {'Authorization': f'Basic {token_enc}'}
    response = app_client.get('/session',  headers=authheaders_token)
    assert response.status_code == 200, f"API did returned a status code of {response.status_code} instad of 200."