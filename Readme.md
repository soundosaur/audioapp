# The audioapp!

[![pylint](https://soundosaur.gitlab.io/soundosaur/audioapp/badges/pylint.svg)](https:/soundosaur.gitlab.io/audioapp/badges/lint/)

### Install:

#### Full:

pip install '.[dev, bot]'

## Audio Files and Streams

use mono, 16khz wav files/streams.

### Convert wav files:

`sox input.wav -r 16000 -c 1 -b 16 disturbence_16000_mono_16bit.wav`

### Use festival locally

`echo "test" | festival_client --server localhost --ttw > /tmp/test.wav`

then use mplayer or other tools to check the frequency, bitrate, channels etc.
