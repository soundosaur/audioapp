;; WARNING: It is inherently insecure to run a festival instance as a
;; server, mainly because it exposes the whole system to exploits which
;; can be easily used by attackers to gain access to your
;; computer. This is because of the inherent design of the festival
;; server. Please use it only in a situation where you are sure that
;; you will not be subjected to such an attack, or have adequate
;; security precautions.

;; This file has been provided as an example file for your use, should
;; you wish to run festival as a server.

;;Maximum number of clients on the server
;;(set! server_max_clients 10)

;;Server port
(set! server_port 1314)

;;Server password:
;;(set! server_passwd "fortesque")

;;Log file location
(set! server_log_file "/var/log/festival/festival.log")

;;Server access list (hosts)
;;Example:
(set! server_access_list '("[^.]+" "127.0.0.1" "localhost.*" "192.168.178.*" "172.*"))
;;Secure default:
;;set! server_access_list '("[^.]+" "127.0.0.1" "localhost"))

;;Server deny list (hosts)

;; Debian-specific: Use aplay to play audio
;;(Parameter.set 'Audio_Command "aplay -q -c 1 -t raw -f s16 -r $SR $FILE")
;;(Parameter.set 'Audio_Method 'Audio_Command)

(Parameter.set 'Wavefiletype 'raw) ;; send back raw wave data

;; Very raw, 16000Hz, 1 ch, s16le
(set! voice_default 'voice_kal_diphone)
;;(set! voice_default 'voice_ked_diphone)

;; English voice, good but not wonderful, 32000 Hz, 1 ch, s16le
;; (set! voice_default 'voice_cmu_us_slt_arctic_hts)

;; Italian voices
;; (set! voice_default 'voice_lp_diphone)
;; (set! voice_default 'voice_pc_diphone)

;; ==== not installed ====
;; English, decent
;; (set! voice_default 'voice_cmu_us_rms_arctic_clunits)

;; Female English, decent
;; (set! voice_default 'voice_cmu_us_slt_arctic_clunits)

;; Canadian voice, good but not wonderful
;; (set! voice_default 'voice_cmu_us_jmk_arctic_clunits)